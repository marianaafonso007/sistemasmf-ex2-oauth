package br.com.sistemamf.agendatelefonica.service;

import br.com.sistemamf.agendatelefonica.model.Agenda;
import br.com.sistemamf.agendatelefonica.repository.AgendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgendaService {
    @Autowired
    private AgendaRepository agendaRepository;

    public Agenda create(Agenda agenda){
        return agendaRepository.save(agenda);
    }

    public List<Agenda> getLitsAgenda(int idUser){
        return agendaRepository.findByUsuarioId(idUser);
    }
}
