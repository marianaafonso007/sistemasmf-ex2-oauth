package br.com.sistemamf.agendatelefonica.repository;

import br.com.sistemamf.agendatelefonica.model.Agenda;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AgendaRepository extends CrudRepository<Agenda, Long> {
    List<Agenda> findByUsuarioId(int usuarioId);
}
