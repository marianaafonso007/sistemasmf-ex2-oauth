package br.com.sistemamf.agendatelefonica.controller;

import br.com.sistemamf.agendatelefonica.model.Agenda;
import br.com.sistemamf.agendatelefonica.security.principal.Usuario;
import br.com.sistemamf.agendatelefonica.service.AgendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cartao")
public class AgendaController {
    @Autowired
    private AgendaService agendaService;

    @PostMapping
    public Agenda create(@RequestBody Agenda agenda, @AuthenticationPrincipal Usuario usuario){
        agenda.setUsuarioId(usuario.getId());
        return agendaService.create(agenda);
    }

    @GetMapping
    public List<Agenda> getAgenda(@AuthenticationPrincipal Usuario usuario){
        return agendaService.getLitsAgenda(usuario.getId());
    }
}
